import { TASK_CLICK } from "../constants/action-types";
export const taskClick = details => ({type: TASK_CLICK, details: details});
