import { TASK_CLICK } from "../constants/action-types";
const initialState = {
  tasks: [
    {id:1, name:"Task1", cost:[
      {name:'Subtask 1',cost:300},
      {name:'Subtask 2', cost:50},
      {name:'Subtask 3', cost:150}
    ]},
    {id:2, name:"Task2", cost:[
      {name:'Subtask 1',cost:200},
      {name:'Subtask 2', cost:1150},
      {name:'Subtask 3', cost:150}
    ]},
    {id:3, name:"Task3", cost:[
      {name:'Subtask 1',cost:400},
      {name:'Subtask 2', cost:50},
      {name:'Subtask 3', cost:150}
    ]}
  ],
  details:{name:"", cost:[]}
};
const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case TASK_CLICK:
      return {...state, details: action.details};
    default:
      return state;
  }
};
export default rootReducer;
