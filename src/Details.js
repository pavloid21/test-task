import React, { Component } from "react";
import { connect } from "react-redux";

class Details extends Component {
  total() {
    let total = 0;
    this.props.data.details.cost.map(task => {
      total += task.cost;
      return null;
    });
    return total;
  }

  render() {
    return (
      <div>
        <h2 className="display-2 bg-secondary text-white">Details</h2>
        <hr />
        <h4>{this.props.data.details.name}</h4>
        <ul className="list-group">
          {this.props.data.details.cost.map(item => {
            return (
              <li key={item.name} className="list-group-item">
                Name: {item.name} Cost:{item.cost}
              </li>
            );
          })}
        </ul>
        <div>
          {this.props.data.details.name} total costs: {this.total()}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    data: state
  };
}

export default connect(mapStateToProps)(Details);
