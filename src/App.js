import React, { Component } from "react";
import { connect } from "react-redux";
import Details from "./Details";
import { taskClick } from "./actions/index";

class App extends Component {
  handleItemClick(e) {
    e.preventDefault();
    this.props.data.tasks.map(item => {
      if (e.target.id == item.id) {
        this.props.dispatch(taskClick(item));
      }
    });
  }

  total() {
    let total = 0;
    this.props.data.tasks.map(task => {
      task.cost.map(item => {
        total += item.cost;
        return null;
      });
      return null;
    });
    console.log("total", total);
    return total;
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-4">
            <h1 className="display-2 bg-secondary text-white">Task list</h1>
            <hr />
            <div className="list-group">
              {this.props.data.tasks.map(task => {
                return (
                  <button
                    key={task.id}
                    id={task.id}
                    onClick={this.handleItemClick.bind(this)}
                    type="button"
                    className="list-group-item list-group-item-action"
                  >
                    {task.name}
                  </button>
                );
              })}
            </div>
          </div>
          <div className="col-8">
            <Details data={this.props.data.details[0]} />
          </div>
        </div>
        Total costs: {this.total()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    data: state
  };
}

export default connect(mapStateToProps)(App);
